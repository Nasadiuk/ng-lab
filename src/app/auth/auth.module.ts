import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthComponent} from './auth.component';
import {AuthRouting} from './auth.routing';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {RestorePasswordComponent} from './components/restore-password/restore-password.component';
import {ForgetPasswordComponent} from './components/forget-password/forget-password.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRouting
  ],
  declarations: [
    AuthComponent,
    LoginComponent,
    RegistrationComponent,
    RestorePasswordComponent,
    ForgetPasswordComponent
  ],
  providers: []
})
export class AuthModule {
}
