export class RegistrationModel {
  public login: string;
  public name: string;
  public surname: string;
  public group: string;
  public email: string;

  constructor(login: string, name: string, surname: string, group: string, email: string) {
    this.login = login;
    this.name = name;
    this.surname = surname;
    this.group = group;
    this.email = email;
  }
}
