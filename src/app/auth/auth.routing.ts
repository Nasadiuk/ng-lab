import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthComponent} from './auth.component';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {ForgetPasswordComponent} from './components/forget-password/forget-password.component';
import {RestorePasswordComponent} from './components/restore-password/restore-password.component';

const AUTH_ROUTES: Routes = [
  {
    path: '', component: AuthComponent, children: [
    {path: '', redirectTo: 'login'},
    {path: 'login', component: LoginComponent},
    {path: 'registration', component: RegistrationComponent},
    {path: 'forgot-password', component: ForgetPasswordComponent},
    {path: 'restore-password', component: RestorePasswordComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(AUTH_ROUTES)],
  exports: [RouterModule]
})
export class AuthRouting {
}

