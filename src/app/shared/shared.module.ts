import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ArticlePreviewComponent} from './components/article/article-preview/article-preview.component';
import {MaterialPreviewComponent} from './components/material/material-preview/material-preview.component';
import {StudentsGroupComponent} from './components/student/students-group/students-group.component';
import {ItemsListComponent} from './components/items-list/items-list.component';
import {ArticleEditingComponent} from './components/article/article-editing/article-editing.component';
import {MaterialEditingComponent} from './components/material/material-editing/material-editing.component';
import {QuestionsListComponent} from './components/question/questions-list/questions-list.component';
import {AddQuestionComponent} from './components/question/add-question/add-question.component';
import {QuestionViewComponent} from './components/question/question-view/question-view.component';
import {StudentPreviewComponent} from './components/student/student-preview/student-preview.component';
import {StudentsListComponent} from './components/student/students-list/students-list.component';
import {StudentEditingComponent} from './components/student/student-editing/student-editing.component';
import {StudentComponent} from '../student/student.component';
import {UIElementsModule} from './ui-elements.module';
import {MainMenuComponent} from './components/main-menu/main-menu.component';

@NgModule({
  imports: [
    CommonModule,
    UIElementsModule
  ],
  exports: [
    UIElementsModule,
    ArticlePreviewComponent,
    ArticleEditingComponent,
    MaterialPreviewComponent,
    MaterialEditingComponent,
    StudentsGroupComponent,
    StudentsListComponent,
    StudentEditingComponent,
    StudentComponent,
    StudentPreviewComponent,
    ItemsListComponent,
    QuestionsListComponent,
    AddQuestionComponent,
    QuestionViewComponent,
    MainMenuComponent
  ],
  declarations: [
    ArticlePreviewComponent,
    ArticleEditingComponent,
    MaterialPreviewComponent,
    MaterialEditingComponent,
    StudentsGroupComponent,
    StudentsListComponent,
    StudentEditingComponent,
    StudentComponent,
    StudentPreviewComponent,
    ItemsListComponent,
    QuestionsListComponent,
    AddQuestionComponent,
    QuestionViewComponent,
    MainMenuComponent
  ]
})
export class AppSharedModule {
}
