import {Component, Input, OnInit} from '@angular/core';
import {ItemListModel} from '../../models/item-list.model';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.scss']
})
export class ItemsListComponent implements OnInit {

  @Input() public itemsList: ItemListModel<any>[];

  constructor() {
  }

  ngOnInit() {
  }

}
