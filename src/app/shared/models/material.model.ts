import {MaterialCategoryEnum} from '../enums/material-category.enum';

export interface IMaterial {
  _id: string;
  title: string;
  materialText: string;
  materialCategory: MaterialCategoryEnum;
  dateCreated: string;
  views: number;
  likes: number;
}

export class MaterialModel implements IMaterial {
  public _id: string;
  public title: string;
  public materialText: string;
  public materialCategory: MaterialCategoryEnum;
  public dateCreated: string;
  public views: number;
  public likes: number;


  constructor(id: string, title: string, materialText: string,
              materialCategory: MaterialCategoryEnum,
              dateCreated: string, views: number,
              likes: number) {
    this._id = id;
    this.title = title;
    this.materialText = materialText;
    this.materialCategory = materialCategory;
    this.dateCreated = dateCreated;
    this.views = views;
    this.likes = likes;
  }
}
