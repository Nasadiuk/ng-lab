export interface IFeedBack {
  _id: string;
  title: string;
  author: string;
  dateCreated: string;
  message: string;
  mark: number;
}

export class FeedbackModel implements IFeedBack {
  public _id: string;
  public title: string;
  public author: string;
  public dateCreated: string;
  public message: string;
  public mark: number;


  constructor(id: string, title: string, author: string,
              dateCreated: string, message: string, mark: number) {
    this._id = id;
    this.title = title;
    this.author = author;
    this.dateCreated = dateCreated;
    this.message = message;
    this.mark = mark;
  }
}
