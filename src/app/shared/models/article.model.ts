import {ArticleCategoryEnum} from '../enums/article-category.enum';

export interface IArticle {
  _id: string;
  title: string;
  articleText: string;
  views: number;
  likes: number;
  creationDate: string;
  category: ArticleCategoryEnum;
}

export class ArticleModel implements IArticle {
  public _id: string;
  public title: string;
  public articleText: string;
  public views: number;
  public likes: number;
  public creationDate: string;
  public category: ArticleCategoryEnum;


  constructor(id: string, title: string, articleText: string,
              views: number, likes: number, creationDate: string,
              category: ArticleCategoryEnum) {
    this._id = id;
    this.title = title;
    this.articleText = articleText;
    this.views = views;
    this.likes = likes;
    this.creationDate = creationDate;
    this.category = category;
  }
}
