import {UserStatusEnum} from '../enums/user-status.enum';
import {TechnologiesEnum} from '../enums/technologies.enum';
import {ArticleModel} from './article.model';
import {IMaterial, MaterialModel} from './material.model';

export interface IStudent {
  firstName: string;
  lastName: string;
  age: string;
  email: string;
  status: UserStatusEnum;
  technologies: TechnologiesEnum[];
  articles: ArticleModel[];
  materials: MaterialModel[];
}

export class StudentModel implements IStudent {
  public firstName: string;
  public lastName: string;
  public age: string;
  public email: string;
  public status: UserStatusEnum;
  public technologies: TechnologiesEnum[];
  public articles: ArticleModel[];
  public materials: MaterialModel[];


  constructor(firstName: string, lastName: string,
              age: string, email: string, status: UserStatusEnum,
              technologies: TechnologiesEnum[], articles: ArticleModel[],
              materials: MaterialModel[]) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.email = email;
    this.status = status;
    this.technologies = technologies;
    this.articles = articles;
    this.materials = materials;
  }
}
