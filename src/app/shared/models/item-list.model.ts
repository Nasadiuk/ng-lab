export class ItemListModel<T> {
  public itemId: string;
  public itemValue: T;


  constructor(itemId: string, itemValue: T) {
    this.itemId = itemId;
    this.itemValue = itemValue;
  }
}
