import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StudentComponent} from './student.component';
import {StudentRouting} from './student.routing';
import {AppSharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    StudentRouting,
    AppSharedModule
  ],
  declarations: [
    StudentComponent
  ]
})
export class StudentModule {
}
