export class ContactBackModel {
  public userName: string;
  public surname: string;
  public title: string;
  public message: string;


  constructor(userName: string, surname: string, title: string, message: string) {
    this.userName = userName;
    this.surname = surname;
    this.title = title;
    this.message = message;
  }
}
