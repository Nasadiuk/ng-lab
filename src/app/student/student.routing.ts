import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MaterialPreviewComponent} from '../shared/components/material/material-preview/material-preview.component';
import {MaterialEditingComponent} from '../shared/components/material/material-editing/material-editing.component';
import {ArticlesListComponent} from '../shared/components/article/articles-list/articles-lst.component';
import {ArticlePreviewComponent} from '../shared/components/article/article-preview/article-preview.component';
import {ArticleEditingComponent} from '../shared/components/article/article-editing/article-editing.component';
import {QuestionsListComponent} from '../shared/components/question/questions-list/questions-list.component';
import {AddQuestionComponent} from '../shared/components/question/add-question/add-question.component';
import {QuestionViewComponent} from '../shared/components/question/question-view/question-view.component';
import {StudentPreviewComponent} from '../shared/components/student/student-preview/student-preview.component';
import {StudentComponent} from './student.component';
import {MaterialsListComponent} from '../shared/components/material/materials-list/materials-list.component';
import {StudentEditingComponent} from '../shared/components/student/student-editing/student-editing.component';

const STUDENT_ROUTES: Routes = [
  {
    path: '', component: StudentComponent, children: [
    {path: '', redirectTo: 'materials'},

    {path: 'materials', component: MaterialsListComponent},
    {path: 'material/:id', component: MaterialPreviewComponent},
    {path: 'material/:id/editing', component: MaterialEditingComponent},

    {path: 'articles', component: ArticlesListComponent},
    {path: 'articles/:id', component: ArticlePreviewComponent},
    {path: 'articles/:id/editing', component: ArticleEditingComponent},

    {path: 'personal-info', component: StudentPreviewComponent},
    {path: 'edit-personal-info', component: StudentEditingComponent},

    {path: 'questions-list', component: QuestionsListComponent},
    {path: 'question/:id/view', component: QuestionViewComponent},
    {path: 'add-question', component: AddQuestionComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(STUDENT_ROUTES)],
  exports: [RouterModule]
})
export class StudentRouting {
}

