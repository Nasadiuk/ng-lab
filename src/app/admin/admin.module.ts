import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin.component';
import {StudentsGroupsListComponent} from './components/students-groups-list/students-groups-list.component';
import {AdminRouting} from './admin.routing';
import {AppSharedModule} from '../shared/shared.module';
import { StudentsGroupEditingComponent } from './components/students-group-editing/students-group-editing.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRouting,
    AppSharedModule
  ],
  declarations: [
    AdminComponent,
    StudentsGroupsListComponent,
    StudentsGroupEditingComponent,
  ],
  providers: []
})
export class AdminModule {
}
