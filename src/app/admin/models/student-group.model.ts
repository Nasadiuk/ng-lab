import {GroupsEnum} from '../enums/groups.enums';
import {StudentModel} from '../../shared/models/student.model';

export interface IStudentGroupModel {
  group: GroupsEnum;
  groupStartDate: Date;
  groupEndDate: Date;
  groupStudents: StudentModel[];
  studentsQuantity: number;
}

export class StudentGroupModel implements IStudentGroupModel {
  public group: GroupsEnum;
  public groupStartDate: Date;
  public groupEndDate: Date;
  public groupStudents: StudentModel[];
  public studentsQuantity: number;


  constructor(group: GroupsEnum, groupStartDate: Date, groupEndDate: Date, groupStudents: StudentModel[], studentsQuantity: number) {
    this.group = group;
    this.groupStartDate = groupStartDate;
    this.groupEndDate = groupEndDate;
    this.groupStudents = groupStudents;
    this.studentsQuantity = studentsQuantity;
  }
}
