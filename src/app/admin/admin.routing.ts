import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AdminComponent} from './admin.component';
import {StudentsGroupsListComponent} from './components/students-groups-list/students-groups-list.component';
import {StudentsGroupComponent} from '../shared/components/student/students-group/students-group.component';
import {MaterialsListComponent} from '../shared/components/material/materials-list/materials-list.component';
import {ArticlePreviewComponent} from '../shared/components/article/article-preview/article-preview.component';
import {ArticlesListComponent} from '../shared/components/article/articles-list/articles-lst.component';
import {ArticleEditingComponent} from '../shared/components/article/article-editing/article-editing.component';
import {MaterialPreviewComponent} from '../shared/components/material/material-preview/material-preview.component';
import {MaterialEditingComponent} from '../shared/components/material/material-editing/material-editing.component';
import {StudentsGroupEditingComponent} from './components/students-group-editing/students-group-editing.component';
import {StudentComponent} from '../student/student.component';
import {StudentsListComponent} from '../shared/components/student/students-list/students-list.component';
import {QuestionsListComponent} from '../shared/components/question/questions-list/questions-list.component';
import {QuestionViewComponent} from '../shared/components/question/question-view/question-view.component';
import {StudentEditingComponent} from '../shared/components/student/student-editing/student-editing.component';

const ADMIN_ROUTES: Routes = [
  {
    path: '', component: AdminComponent, children: [
    {path: '', redirectTo: 'students-groups'},

    {path: 'students-groups', component: StudentsGroupsListComponent},
    {path: 'students-group/:id', component: StudentsGroupComponent},
    {path: 'students-group/:id/editing', component: StudentsGroupEditingComponent},

    {path: 'students-list', component: StudentsListComponent},
    {path: 'student/:id', component: StudentComponent},
    {path: 'student/:id/editing', component: StudentEditingComponent},

    {path: 'materials-list', component: MaterialsListComponent},
    {path: 'material/:id/preview', component: MaterialPreviewComponent},
    {path: 'material/:id/editing', component: MaterialEditingComponent},

    {path: 'articles', component: ArticlesListComponent},
    {path: 'article/:id/preview', component: ArticlePreviewComponent},
    {path: 'article/:id/editing', component: ArticleEditingComponent},

    {path: 'questions-list', component: QuestionsListComponent},
    {path: 'question/:id/view', component: QuestionViewComponent},
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(ADMIN_ROUTES)],
  exports: [RouterModule]
})
export class AdminRouting {
}

