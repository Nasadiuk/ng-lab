import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';


const routes: Routes = [
  {path: '', component: AppComponent},
  {path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule'},
  {path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule'},
  {path: 'student', loadChildren: 'app/student/student.module#StudentModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}


